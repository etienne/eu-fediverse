## EU Commission for Fediverse

This repository provides a draft written question to send to the EU Commission: communication over self-hosted services (Fediverse) in addition to major commercial platforms should be enabled.

The draft written question can be found [here](letter.md).
